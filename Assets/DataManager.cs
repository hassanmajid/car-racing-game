using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;

    private GameLevelModel _gameLevelModel;
    
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else {
            Destroy(this);
        }
        
        DontDestroyOnLoad(this);
    }


    public void SetGameLevelModel(GameLevelModel gameLevelModel) {
        _gameLevelModel = gameLevelModel;
    }

    public GameLevelModel GetGameLevelModel() {
        
        if (_gameLevelModel == null)
            _gameLevelModel = GameLevelModel.WuJing;
        
        return _gameLevelModel;
    }


    private string playerName;

    public string GetPlayerName() {
        return playerName;
    }

    public void SetPlayerName(string val) {
        playerName = val;
    }
    
    
    private int index;

    public int GetPlayerIndex() {
        return index;
    }

    public void SetPlayerIndex(int val) {
        index = val;
    }
    
    
    private int scene;

    public int GetPlayerScene() {
        return scene;
    }

    public void SetPlayerScene(int val) {
        scene = val;
    }

}
