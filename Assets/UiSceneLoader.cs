using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UiSceneLoader : MonoBehaviour
{
    public Action<int,Transform> modelChange;
    public Action<bool> SetPlatformStatus;
    public Action<SwipeDetector.DraggedDirection> swipeAction;
    
    [SerializeField] private Transform platform;
    [SerializeField] private LoadingPage _loadingPage;
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private Transform playerPosition;
    
    
    private LoadingPage LoadingPage;
    
    public static UiSceneLoader instance;

    [SerializeField] private Transform parent;
    [SerializeField] private MainInterfaceControllerUpdated _mainInterfaceControllor;
    private MainInterfaceControllerUpdated MainInterfaceControllor;


    [SerializeField] private Inventory _inventory;
    private Inventory Inventory;


    [SerializeField] private ShopScript _shopScript;
    private ShopScript ShopScript;
    
    
    
    
    private void Start() {
        modelChange += RotatePlatform;
        SetPlatformStatus += SetPlatformStatusMethod;
    }


    private void OnDestroy() {
        modelChange -= RotatePlatform;
        SetPlatformStatus-= SetPlatformStatusMethod;
    }

    void SetPlatformStatusMethod(bool status) {
        
        platform.gameObject.SetActive(status);

        if (DataManager.instance.GetGameLevelModel() == GameLevelModel.WuJing) {
            GetMainInterfaceController().SetMapsStatus(true);
        }
        else {
            GetMainInterfaceController().SetMapsStatus(false);
        }
    }
    private Coroutine lerpCorot;
    void RotatePlatform(int index,Transform currentPlayer) {
        Transform to = platform;
     if(lerpCorot!=null)
         StopCoroutine(lerpCorot);
     currentPlayer.DOScale(new Vector3(10, 10, 10)*_playerModel.playerDataList[index].scaleFactor, 0.5f).SetEase(Ease.OutBack);
     
     lerpCorot=StartCoroutine(LerpAngle(platform, 2,index));
        
    }

    public float speed=1;
    IEnumerator LerpAngle(Transform current, float totalTime,int index) {

        float currentTime = 0;

        while (currentTime < totalTime) {
            currentTime += Time.deltaTime;
            platform.rotation = Quaternion.Lerp(current.rotation,Quaternion.Euler(current.rotation.x, _playerModel.playerDataList[index].playerPosition,current.rotation.z), speed*currentTime);
            yield return null;
        }
    }
    
    
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else {
            Destroy(this);
        }

       

        var load = GetLoadingReference();
        load.InitScene();
        GlobalConst.SceneName = SceneType.UIScene;
      
        ItemData.Instance.RefreshData();
    }

    
    
    public Inventory GetInventory() {
        
        if (Inventory == null) {
            Inventory = Instantiate(_inventory, parent);
        }

        return Inventory;
    }
    
    public ShopScript GetShop() {
        
        if (ShopScript == null) {
            ShopScript = Instantiate(_shopScript, parent);
        }

        return ShopScript;
    }
    
    
    
  
    public MainInterfaceControllerUpdated GetMainInterfaceController() {
        if (MainInterfaceControllor == null) {
            MainInterfaceControllor = Instantiate(_mainInterfaceControllor, parent);
            MainInterfaceControllor.RegisterMethods();
        }

        return MainInterfaceControllor;
    }
    void SpawnLoading() {
        LoadingPage=Instantiate(_loadingPage, parent);
    }
    
    public LoadingPage GetLoadingReference() {
       
        if(LoadingPage==null)
            SpawnLoading();
        GlobalConst.SceneName = SceneType.GameScene;
        
        return LoadingPage;
    }

    [SerializeField] List<GameObject> _playerModels;
    private List<AnimatorManager> _playerModelsAnimations;

    public void SpawnPlayers() {
       platform.gameObject.SetActive(true);
        _playerModelsAnimations = new List<AnimatorManager>();
        // Quaternion mov= Quaternion.Euler(0,200,0);
        // for (int i = 0; i < _playerModel.playerDataList.Count; i++) {
        //     var player = Instantiate(_playerModel.playerDataList[i].playerModel,Vector3.zero, mov);
        //     _playerModels.Add(player);
        //     _playerModelsAnimations.Add(player.transform.GetChild(1).GetComponent<AnimatorManager>());
        //     player.transform.GetChild(1).GetComponent<AnimatorManager>().Init();
        //     if(i!=0)
        //         player.SetActive(false);
        // }

        for (int i = 0; i < _playerModels.Count; i++) {
            if (i == 0)
                _playerModels[i].transform.DOScale(new Vector3(10, 10, 10)*_playerModel.playerDataList[i].scaleFactor, 0);
            
            
            _playerModelsAnimations.Add(_playerModels[i].transform.GetChild(1).GetComponent<AnimatorManager>());
            _playerModels[i].transform.GetChild(1).GetComponent<AnimatorManager>().Init();
        }
        
        
        
    }

    public List<GameObject> GetPlayerList() {
        return _playerModels;
    }
    
    public List<AnimatorManager> GetPlayerAnimationsList() {
        return _playerModelsAnimations;
    }
    
    
}
