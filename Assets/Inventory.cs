using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Text shieldCount;
    [SerializeField] private Text speedCount;
    [SerializeField] private Text bombCount;

    [SerializeField] GameObject inventoryPopUp;
    [SerializeField] AnimationCurve inventoryEase;

    private void Awake()
    {
        inventoryPopUp.transform.DOScale(0, 0);
    }
    private void OnEnable() {
        inventoryPopUp.transform.DOScale(1, 0.5f).SetEase(inventoryEase);
        Initialize();
    }
    private void OnDisable()
    {
        inventoryPopUp.transform.DOScale(0, 0.5f).SetEase(Ease.Linear);
    }


    void Initialize() {
        shieldCount.text=PlayerData.Instance.GetItemNum(PlayerData.ItemType.ProtectShield).ToString();
        speedCount.text=PlayerData.Instance.GetItemNum(PlayerData.ItemType.SpeedUp).ToString();
        bombCount.text=PlayerData.Instance.GetItemNum(PlayerData.ItemType.FlyBomb).ToString();
    }

    public void SetInventoryPanelOff() {
        this.gameObject.SetActive(false);
        UiSceneLoader.instance.SetPlatformStatus?.Invoke(true);
    }
    
    public void SetInventoryPanelOn() {
        this.gameObject.SetActive(true);
    }


    public void OpenShop() {
        SetInventoryPanelOff();
        UiSceneLoader.instance.GetMainInterfaceController().OpenShop();
    }
    
}
