using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerModel", menuName = "ScriptableObjects/PlayerModel", order = 1)]
public class PlayerModel : ScriptableObject
{
   [SerializeField] public List<PlayerModelDetails> playerDataList ;

   [SerializeField] public ShopPowerUps ShopPowerUps;
   [SerializeField] public List<Maps> MapsList ;
   
}


[System.Serializable]
public class PlayerModelDetails
{
    [SerializeField]
    public int scaleFactor;
    [SerializeField]
    public float playerPosition;
    [SerializeField] public GameObject playerModel;
    [SerializeField]public string Name;
    [SerializeField] public string description;
    [SerializeField] public float coinPrice;
    [SerializeField] public float gemPrice;
    [SerializeField] public bool isCoin;
    [SerializeField] public int jsonVal;
    
}

[System.Serializable]
public class ShopPowerUps
{
    [SerializeField]
    public int speedUpCost;
    
    [SerializeField]
    public int shieldCost;
    
    [SerializeField] public int bombCost;
    
    
}

[System.Serializable]
public class Maps
{
    [SerializeField]
    public Sprite mapSprite;
    
    [SerializeField]
    public float mapPos;
    
    [SerializeField]
    public float mapPrice;

}



