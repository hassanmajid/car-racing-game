using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public class Modes
{
    public float Xpos;
    public GameObject mode;
}

///////////////////////////////////////////////////////////////////////////////////

public enum MainMenuAnimations { Rotate, Shake, PopInOut };
[System.Serializable]
public struct MenuAnimations
{
    [SerializeField] public GameObject animeBtns;
    [SerializeField] public MainMenuAnimations menuAnims;
    [SerializeField] public int animationDelay;
    [SerializeField] public int timeToCompleteAnim;
}

public enum BottomBtnAnimType { ShakeBtn, RotateBtn }
[System.Serializable]
public struct ButtonButtonsAnimations
{
    [SerializeField] public GameObject buttomButtons;
    [SerializeField] public GameObject buttonReferance;
    [SerializeField] public BottomBtnAnimType btnAnim;
    [SerializeField] public float animDelay;
    [SerializeField] public float completeTime;
}

///////////////////////////////////////////////////////////////////////////////////

public class MainInterfaceControllerUpdated : MonoBehaviour
{
    [SerializeField] MenuAnimations[] menuAnimations;
    private bool isAnimate;

    [SerializeField] ButtonButtonsAnimations[] buttonButtonsAnimations;
    private bool bottomAnimate;

    [SerializeField] GameObject startGameButton;
    private float buttonMoveTime;

    [SerializeField] AnimationCurve easeCurve;



    ///////////////////////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject mapSelectButton;
    [SerializeField] private GameObject mapBuyingButton;
    [SerializeField] private Text mapBuyingAmount;
    //[SerializeField] private List<Modes> modes;
    [SerializeField] private Transform modeParent;


    public static MainInterfaceControllerUpdated instance;

    [SerializeField] private GameObject mapsButton;

    [SerializeField] private GameObject activeButton;
    [SerializeField] private GameObject selectButton;



    [SerializeField] private GameObject mapsPanel;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private GameObject modesPanel;
    [SerializeField] private Button ClassicGameBtn;
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private Text coinText;
    [SerializeField] private Text gemText;
    [SerializeField] private Text nameText;
    [SerializeField] private Text descriptionText;
    [SerializeField] private GameObject coinButton;
    [SerializeField] private GameObject gemButton;
    [SerializeField] private Text coinsValue;
    [SerializeField] private Text gemsValue;

    [SerializeField] private Text Egg;
    [SerializeField] private Text Level;
    [SerializeField] private Text Mode;

    public GameObject currentPlayer;

    private int selectedIndex;

    [SerializeField] private Mapcontent mapDetail;
    [SerializeField] private Transform mapParent;


    private int currentMapIndex;
    public int selectedMapIndex;

    private List<GameObject> mapList;

    public void SetMapPrice()
    {
        PlayerPrefs.SetInt(_playerModel.MapsList[currentMapIndex].mapSprite.name, 1);
        PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin, (int)_playerModel.MapsList[currentMapIndex].mapPrice);
        coinsValue.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin).ToString();
        var map = mapList[currentMapIndex].transform.GetChild(0);
        map.gameObject.SetActive(false);
        UpdateMapVal();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void Start()
    {
        startGameButton.SetActive(false);
        startGameButton.transform.DOScale(0, 0);
        isAnimate = true;
        bottomAnimate=true;
        buttonMoveTime = 0.2f;

        modesPanel.transform.DOScale(0.4f, 0);

        Invoke(nameof(StartButtonAnimation), 1);
        Invoke(nameof(BottomButtonAnimations), 1);
        Invoke(nameof(TopBarAnimations), 2);
    }

    //////////////////////////////////////////////////////              Top Bar Animations          //////////////////////////////////////////////////////


    private void TopBarAnimations()
    {
        for(int i = 0; i < menuAnimations.Length; i++)
        {
            StartCoroutine(TopBarMenuAnimations(menuAnimations[i].animeBtns, menuAnimations[i].menuAnims, menuAnimations[i].timeToCompleteAnim, menuAnimations[i].animationDelay));
        }
    }

    IEnumerator TopBarMenuAnimations(GameObject btnname, MainMenuAnimations btns,int completionTime,int delay)
    {
        while (isAnimate)
        {
            switch (btns)
            {
                case MainMenuAnimations.Rotate:
                    btnname.transform.DOLocalRotate(new Vector3(0, 180, 0), completionTime).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        btnname.transform.DOLocalRotate(new Vector3(0, 0, 0), completionTime);
                    });
                    yield return new WaitForSeconds(delay);
                    break;
                case MainMenuAnimations.Shake:
                    btnname.transform.DOShakeScale(completionTime, 0.3f, 10, 50).SetEase(Ease.Linear);
                    yield return new WaitForSeconds(delay);
                    break;
                case MainMenuAnimations.PopInOut:
                    btnname.transform.DOScale(0.7f, completionTime).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        btnname.transform.DOScale(0.6f, completionTime);
                    });
                    yield return new WaitForSeconds(delay);
                    break;
                default:
                    break;
            }
        }
    }

    //////////////////////////////////////////////////////              Bottom Buttons Animations          //////////////////////////////////////////////////////

    private void StartButtonAnimation()
    {
        startGameButton.SetActive(true);
        startGameButton.transform.DOScale(1, 0.8f).SetEase(easeCurve);
        startGameButton.transform.DOScale(1.04f, 0.4f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetDelay(3).OnComplete(() =>
        {
            startGameButton.transform.DOScale(1, 0.3f);
        });
    }
    private void BottomButtonAnimations()
    {
        Debug.Log(buttonButtonsAnimations.Length);
        for(int i = 0; i < buttonButtonsAnimations.Length; i++)
        {
            buttonButtonsAnimations[i].buttomButtons.transform.DOMove(buttonButtonsAnimations[i].buttonReferance.transform.position, buttonMoveTime * (i + 1)).SetEase(easeCurve).SetDelay(buttonButtonsAnimations[i].animDelay + 0.5f);
            Debug.Log(buttonMoveTime);
        }
        Invoke(nameof(PlayRotateAnimationOnBottomButtons), 3);
    }
    private void PlayRotateAnimationOnBottomButtons()
    {
        for (int i = 0; i < buttonButtonsAnimations.Length; i++)
        {
            StartCoroutine(ButtomButtonsAnim(buttonButtonsAnimations[i].buttomButtons, buttonButtonsAnimations[i].btnAnim, buttonButtonsAnimations[i].completeTime, buttonButtonsAnimations[i].animDelay));
        }
    }
    IEnumerator ButtomButtonsAnim(GameObject name, BottomBtnAnimType animType, float complaetion, float animDelay)
    {
        while (bottomAnimate)
        {
            switch (animType)
            {
                case BottomBtnAnimType.ShakeBtn:
                    name.transform.DOShakeScale(complaetion, 0.3f, 10, 50).SetEase(Ease.Linear);
                    yield return new WaitForSeconds(animDelay);
                    break;
                case BottomBtnAnimType.RotateBtn:
                    name.transform.DORotate(new Vector3(0, 0, -3), complaetion).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        name.transform.DORotate(new Vector3(0, 0, 3), complaetion);
                    });
                    yield return new WaitForSeconds(animDelay);
                    break;
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void CloaseModesPanel()
    {
        modesPanel.SetActive(false);
    }
    public void CloseMapsPanel()
    {
        if (currentPlayer == null)
            currentPlayer = UiSceneLoader.instance.GetPlayerList()[currentIndex];

        UiSceneLoader.instance.SetPlatformStatus?.Invoke(true);
        gamePanel.SetActive(true);
        //mapsPanel.transform.DOScale(0, 1).SetEase(Ease.Linear);
        mapsPanel.SetActive(false);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    void UpdateMapVal()
    {

        var val = PlayerPrefs.GetInt(_playerModel.MapsList[currentMapIndex].mapSprite.name, -1);
        if (val == 1 || _playerModel.MapsList[currentMapIndex].mapPrice == 0)
        {

            mapSelectButton.SetActive(true);
            mapBuyingButton.SetActive(false);
        }
        else
        {
            mapSelectButton.SetActive(false);
            mapBuyingButton.SetActive(true);
            mapBuyingAmount.text = _playerModel.MapsList[currentMapIndex].mapPrice.ToString();

            if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin) >= _playerModel.MapsList[currentMapIndex].mapPrice)
            {
                mapBuyingButton.GetComponent<Button>().interactable = true;

            }
            else
            {
                mapBuyingButton.GetComponent<Button>().interactable = false;

            }

            var map = mapList[currentMapIndex].transform.GetChild(0);
            map.gameObject.SetActive(true);
        }
    }

    void CheckMap(int i, GameObject map)
    {
        var val = PlayerPrefs.GetInt(_playerModel.MapsList[i].mapSprite.name, -1);
        if (val == 1 || _playerModel.MapsList[i].mapPrice == 0)
        {
            map.SetActive(false);
        }
        else
        {

            map.gameObject.SetActive(true);
        }
    }


    void SpawnMaps()
    {
        mapList = new List<GameObject>();
        for (int i = 0; i < _playerModel.MapsList.Count; i++)
        {
            var map = Instantiate(mapDetail, mapParent);
            map.SetMapImage(_playerModel.MapsList[i].mapSprite);
            mapList.Add(map.gameObject);
            CheckMap(i, map.transform.GetChild(0).gameObject);
        }




        // McveMapPos();
        //mapParent.localPosition = new Vector3(_playerModel.MapsList[currentMapIndex].mapPos, mapParent.localPosition.y, mapParent.localPosition.z);

        //modeParent.localPosition = new Vector3(modes[currentModeIndex].Xpos, modeParent.localPosition.y, modeParent.localPosition.z);


    }

    public void SelectCurrentMap()
    {
        selectedMapIndex = currentIndex;
        DataManager.instance.SetPlayerScene(currentIndex++);
        CloseModes();
    }

    private Coroutine mapCor;
    private Coroutine modeCor;
    void McveMapPos(int index)
    {
        if (mapCor != null)
        {
            StopCoroutine(mapCor);
        }
        UpdateMapVal();
        mapCor = StartCoroutine(LerpPositon(mapList[index].transform, mapList[currentMapIndex].transform, 1, mapParent, _playerModel.MapsList[currentMapIndex].mapPos));

    }

    void MoveModePos(int index)
    {
        if (modeCor != null)
        {
            StopCoroutine(modeCor);
        }

        //modeCor = StartCoroutine(LerpPositon(modes[index].mode.transform, modes[currentModeIndex].mode.transform, 1, modeParent, modes[currentModeIndex].Xpos));

    }



    public float speed = 1;
    private TweenerCore<Vector3, Vector3, VectorOptions> scaleTween;
    private TweenerCore<Quaternion, Vector3, QuaternionOptions> rotateTween;


    IEnumerator LerpPositon(Transform prev, Transform result, int time, Transform platform, float platformVal)
    {

        if (scaleTween != null && scaleTween.IsPlaying())
        {
            scaleTween.Kill();
        }

        if (rotateTween != null && rotateTween.IsPlaying())
        {
            rotateTween.Kill();
        }

        float currentTime = 0;

        prev.localScale = new Vector3(1, 1, 1);

        prev.localRotation = Quaternion.Euler(prev.localRotation.x, 40f,
             prev.localRotation.z);


        rotateTween = result
           .DOLocalRotate(
               new Vector3(result.localRotation.x, 0,
                   result.transform.localRotation.z), 0.5f);


        scaleTween = result.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f).SetEase(Ease.OutBack);

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            platform.localPosition = Vector3.Lerp(platform.localPosition, new Vector3(platformVal, platform.localPosition.y, platform.localPosition.z), speed * currentTime);
            // platform.rotation = Quaternion.Lerp(current.rotation,Quaternion.Euler(current.rotation.x, _playerModel.playerDataList[index].playerPosition,current.rotation.z), speed*currentTime);
            yield return null;
        }

    }

    void NextMap()
    {
        int index;

        if (currentMapIndex < _playerModel.MapsList.Count - 1)
        {
            currentMapIndex++;
            index = currentMapIndex - 1;
        }
        else
        {
            currentMapIndex = 0;
            index = _playerModel.MapsList.Count - 1;
        }

        McveMapPos(index);

    }

    void PrevMap()
    {
        int index;
        if (currentMapIndex > 0)
        {
            currentMapIndex--;
            index = currentMapIndex + 1;
        }
        else
        {
            currentMapIndex = _playerModel.MapsList.Count - 1;
            index = 0;
        }

        McveMapPos(index);


    }


    private int currentModeIndex;
    void NextMode()
    {


        int index;

        //if (currentModeIndex < modes.Count - 1)
        //{
        //    currentModeIndex++;
        //    index = currentModeIndex - 1;
        //}
        //else
        //{
        //    currentModeIndex = 0;
        //    index = modes.Count - 1;
        //}




        //MoveModePos(index);

    }

    void PrevMode()
    {
        int index;
        if (currentModeIndex > 0)
        {
            currentModeIndex--;
            index = currentModeIndex + 1;
        }
        else
        {
            //currentModeIndex = modes.Count - 1;
            index = 0;
        }

        MoveModePos(index);

    }

    public void SetMapsStatus(bool status)
    {
        mapsButton.GetComponent<Button>().interactable = status;
    }


    public void OpenInventory()
    {
        var inventory = UiSceneLoader.instance.GetInventory();
        if (!inventory.gameObject.activeInHierarchy)
        {
            inventory.gameObject.SetActive(true);
        }
        UiSceneLoader.instance.SetPlatformStatus?.Invoke(false);
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DataManager.instance.SetGameLevelModel(GameLevelModel.WuJing);
        SpawnMaps();

    }

    private void OnEnable()
    {
        coinsValue.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin).ToString();
        gemsValue.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.Jewel).ToString();
        Egg.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.ColorEgg).ToString();
        Level.text = PlayerData.Instance.GetSelectedLevel().ToString();

        //   GameData.Instance.curEggCount.ToString();

        UpdateButton();

        if (DataManager.instance.GetGameLevelModel() == GameLevelModel.WuJing)
        {
            SetMapsStatus(true);
            Mode.text = "Classic";
        }
        else
        {
            SetMapsStatus(false);
            Mode.text = DataManager.instance.GetGameLevelModel().ToString();
        }
    }

    private int currentIndex = 0;

    void UpdatePlayers(int index)
    {
        // foreach (var VARIABLE in UiSceneLoader.instance.GetPlayerList()) {
        //     VARIABLE.SetActive(false);
        // }
        //
        if (currentPlayer != null)
            currentPlayer.transform.DOScale(new Vector3(70, 70, 70), 0.2f).SetEase(Ease.InBack);
        else
        {
            UiSceneLoader.instance.GetPlayerList()[0].transform.DOScale(new Vector3(70, 70, 70), 0.2f).SetEase(Ease.InBack);
        }

        var Player = UiSceneLoader.instance.GetPlayerList()[index];
        // Player.SetActive(true);
        currentPlayer = Player;
        UpdateButton();
        UiSceneLoader.instance.modelChange?.Invoke(currentIndex, currentPlayer.transform);
    }

    public void Selected()
    {
        selectedIndex = currentIndex;
        activeButton.SetActive(true);
        selectButton.SetActive(false);
    }


    public void RegisterMethods()
    {
        UiSceneLoader.instance.swipeAction += SwipeCharacters;
    }

    private void OnDestroy()
    {
        UiSceneLoader.instance.swipeAction -= SwipeCharacters;
    }

    void SwipeCharacters(SwipeDetector.DraggedDirection direction)
    {
        if (direction == SwipeDetector.DraggedDirection.Left)
        {
            if (gamePanel.activeInHierarchy)
            {
                RightButton();
            }
            else if (mapsPanel.activeInHierarchy)
            {
                NextMap();
            }
            else if (modesPanel.activeInHierarchy)
            {
                NextMode();
            }

        }
        else if (direction == SwipeDetector.DraggedDirection.Right)
        {
            if (gamePanel.activeInHierarchy)
            {
                LeftButton();
            }
            else if (mapsPanel.activeInHierarchy)
            {
                PrevMap();
            }
            else if (modesPanel.activeInHierarchy)
            {
                PrevMode();
            }
        }
    }

    public void OpenShop()
    {
        CloseModes();
        UiSceneLoader.instance.SetPlatformStatus?.Invoke(false);
        UiSceneLoader.instance.GetShop().gameObject.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void LeftButton()
    {
        if (currentIndex > 0)
        {
            currentIndex--;

        }
        else
        {
            currentIndex = UiSceneLoader.instance.GetPlayerList().Count - 1;
        }
        UpdatePlayers(currentIndex);
    }

    public void RightButton()
    {
        if (currentIndex < UiSceneLoader.instance.GetPlayerList().Count - 1)
        {
            currentIndex++;
        }
        else
        {
            currentIndex = 0;
        }
        UpdatePlayers(currentIndex);
    }

    public void SetGameLevelMode(int val)
    {

        if (val == 1)
        {
            DataManager.instance.SetGameLevelModel(GameLevelModel.Rank);
        }
        else if (val == 2)
        {
            DataManager.instance.SetGameLevelModel(GameLevelModel.DualMeet);
        }
        else if (val == 3)
        {
            DataManager.instance.SetGameLevelModel(GameLevelModel.Weedout);
        }
        else if (val == 4)
        {
            DataManager.instance.SetGameLevelModel(GameLevelModel.LimitTime);
        }
        else
        {
            DataManager.instance.SetGameLevelModel(GameLevelModel.WuJing);
        }

        if (val == 5)
        {
            Mode.text = "Classic";
        }
        else
        {
            Mode.text = DataManager.instance.GetGameLevelModel().ToString();
        }

        CloseModes();
    }

    public void OpenCars()
    {
        if (!gamePanel.activeInHierarchy)
        {
            CloseModes();
        }

    }


    public void OpenModes()
    {

        if (currentPlayer == null)
            Debug.Log(">>>>>>>>>   Null Data");
            currentPlayer = UiSceneLoader.instance.GetPlayerList()[currentIndex];

        UiSceneLoader.instance.SetPlatformStatus?.Invoke(false);
        gamePanel.SetActive(false);
        modesPanel.SetActive(true);
        modesPanel.transform.DOScale(1, 0.5f).SetEase(easeCurve);
        mapsPanel.SetActive(false);
        mapsButton.GetComponent<Button>().interactable = false;
    }

    public void OpenMaps()
    {

        if (currentPlayer == null)
            currentPlayer = UiSceneLoader.instance.GetPlayerList()[currentIndex];

        UiSceneLoader.instance.SetPlatformStatus?.Invoke(false);
        gamePanel.SetActive(false);
        modesPanel.SetActive(false);
        mapsPanel.transform.DOScale(1, 1).SetEase(Ease.OutBounce);
        mapsPanel.SetActive(true);

    }

    public void CloseModes()
    {
        //currentPlayer.SetActive(true);
        UiSceneLoader.instance.SetPlatformStatus?.Invoke(true);
        modesPanel.transform.DOScale(0, 1).SetEase(Ease.Linear);
        modesPanel.SetActive(false);
        gamePanel.SetActive(true);
        mapsPanel.SetActive(false);
        //mapsButton.GetComponent<Button>().interactable = true;
    }



    public void ClassicGameButton()
    {
        AudioManger.Instance.PlaySound(AudioManger.SoundName.ButtonClick);
        //PlayerData.Instance.SetGameMode ("WuJin");
        DataManager.instance.SetGameLevelModel(DataManager.instance.GetGameLevelModel());
        PlayerData.Instance.SetSelectedModel(_playerModel.playerDataList[selectedIndex].jsonVal);

        PlayerData.Instance.SetSelectModelIsTestDrive(false);

        PlayerData.Instance.SaveData();

        GlobalConst.SceneName = SceneType.GameScene;
        GlobalConst.FirstIn = true;
        gameObject.SetActive(false);

        // var player = UiSceneLoader.instance.GetPlayerList()[selectedIndex];
        // player.SetActive(false);
        UiSceneLoader.instance.SetPlatformStatus?.Invoke(false);
        DataManager.instance.SetPlayerName(_playerModel.playerDataList[currentIndex].Name.ToString());
        DataManager.instance.SetPlayerIndex(selectedIndex);
        var loadingPage = UiSceneLoader.instance.GetLoadingReference();
        // loadingPage.InitScene();
        loadingPage.LoadScene();
        //classGameBtnParticle.Play();

    }


    void PlayModelAnim()
    {
        var anim = UiSceneLoader.instance.GetPlayerAnimationsList()[currentIndex];
        anim.Show();
    }

    void ShowBeforeAnim()
    {
        //modelAnimArr [modelIndex].BeginShow ();
        CancelInvoke("PlayModelAnim");
        InvokeRepeating("PlayModelAnim", 0, Random.Range(3.5f, 5.0f));
    }


    public void BuyButton()
    {
        if (_playerModel.playerDataList[currentIndex].isCoin)
        {
            if (_playerModel.playerDataList[currentIndex].coinPrice <=
                PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin))
            {

                PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin, (int)_playerModel.playerDataList[currentIndex].coinPrice);
                ClassicGameBtn.interactable = true;
                coinsValue.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin).ToString();
                UnlockPlayer(_playerModel.playerDataList[currentIndex].playerModel.name);
                //activeButton.SetActive(true);
                coinButton.SetActive(false);
                activeButton.SetActive(false);
                selectButton.SetActive(true);
            }
            else
            {
                ClassicGameBtn.interactable = false;
            }
        }
        else
        {
            if (_playerModel.playerDataList[currentIndex].gemPrice <=
                PlayerData.Instance.GetItemNum(PlayerData.ItemType.Jewel))
            {
                PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin, (int)_playerModel.playerDataList[currentIndex].gemPrice);
                ClassicGameBtn.interactable = true;
                gemsValue.text = PlayerData.Instance.GetItemNum(PlayerData.ItemType.Jewel).ToString();
                UnlockPlayer(_playerModel.playerDataList[currentIndex].playerModel.name);
                gemButton.SetActive(false);
                activeButton.SetActive(false);
                selectButton.SetActive(true);
            }
            else
            {
                ClassicGameBtn.interactable = false;
            }
        }

    }


    void UnlockPlayer(string name)
    {
        PlayerPrefs.SetInt(name, 1);
    }



    void UpdateButton()
    {
        coinText.text = _playerModel.playerDataList[currentIndex].coinPrice.ToString();
        gemText.text = _playerModel.playerDataList[currentIndex].gemPrice.ToString();
        nameText.text = _playerModel.playerDataList[currentIndex].Name.ToString();
        descriptionText.text = _playerModel.playerDataList[currentIndex].description.ToString();

        if (_playerModel.playerDataList[currentIndex].isCoin)
        {
            gemButton.SetActive(false);
            coinButton.SetActive(true);
        }
        else
        {
            gemButton.SetActive(true);
            coinButton.SetActive(false);
        }

        var playerVal = PlayerPrefs.GetInt(_playerModel.playerDataList[currentIndex].playerModel.name, -1);
        if (_playerModel.playerDataList[currentIndex].isCoin)
        {
            if (playerVal == 1 || _playerModel.playerDataList[currentIndex].coinPrice == 0)
            {
                ClassicGameBtn.interactable = true;
                if (selectedIndex == currentIndex)
                {
                    activeButton.SetActive(true);
                    selectButton.SetActive(false);
                }
                else
                {
                    activeButton.SetActive(false);
                    selectButton.SetActive(true);
                }

                coinButton.SetActive(false);
                gemButton.SetActive(false);
            }
            else
            {
                ClassicGameBtn.interactable = false;
                selectButton.SetActive(false);
                activeButton.SetActive(false);
                coinButton.SetActive(true);
                gemButton.SetActive(false);
            }
        }
        else
        {
            if (playerVal == 1 || _playerModel.playerDataList[currentIndex].gemPrice == 0)
            {
                ClassicGameBtn.interactable = true;
                if (selectedIndex == currentIndex)
                {
                    activeButton.SetActive(true);
                    selectButton.SetActive(false);
                }
                else
                {
                    activeButton.SetActive(false);
                    selectButton.SetActive(true);
                }

                coinButton.SetActive(false);
                gemButton.SetActive(false);
            }
            else
            {
                ClassicGameBtn.interactable = false;
                gemButton.SetActive(true);
                coinButton.SetActive(false);
                selectButton.SetActive(false);
                activeButton.SetActive(false);
            }
        }





        ShowBeforeAnim();
    }




}
