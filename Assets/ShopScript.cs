using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
   [SerializeField] private Text warningText;

   [SerializeField] private GameObject warningPanel;


   public void HideShop() {
      this.gameObject.SetActive(false);
      UiSceneLoader.instance.GetMainInterfaceController().gameObject.SetActive(true);
      UiSceneLoader.instance.SetPlatformStatus?.Invoke(true);
   }
   
   void ShowWarningPanel(string war) {
      warningText.text = war;
      warningPanel.SetActive(true);
   }


   public void HideWarningPanel() {
      warningPanel.SetActive(false);
   }


   public void BuyShield() {
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Jewel)>=5) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Jewel,5);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.ProtectShield,1);
      }
      else {
         ShowWarningPanel("Not Enough Jewels");
      }
      
   }
   
   public void BuyBomb() {
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Jewel)>=15) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Jewel,15);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.FlyBomb,1);
      }
      else {
         ShowWarningPanel("Not Enough Jewels");
      }
      
   }
   
   public void BuySpeed() {
      
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin)>=1000) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin,1000);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.SpeedUp,1);
      }
      else {
         ShowWarningPanel("Not Enough Coins");
      }

   }

   public void Buy12Jewels() {
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin)>=1500) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin,1500);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.Jewel,12);
      }
      else {
         ShowWarningPanel("Not Enough Coins");
      }
   }
   
   
   public void Buy120Jewels() {
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin)>=12000) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin,12000);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.Jewel,120);
      }
      else {
         ShowWarningPanel("Not Enough Coins");
      }
   }
   
   public void Buy240Jewels() {
      if (PlayerData.Instance.GetItemNum(PlayerData.ItemType.Coin)>=25000) {
         PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Coin,25000);
         PlayerData.Instance.AddItemNum(PlayerData.ItemType.Jewel,240);
      }
      else {
         ShowWarningPanel("Not Enough Coins");
      }
   }
   
   
   public void ShieldFor10Dollars() {
      
      
      
      
   }
   
   public void SpeedFor10Dollars() {
      
      
      
      
   }
   
   public void BombFor10Dollars() {
      
      
      
      
   }
   
   
   
   

  
   
   
   
   
   
   
   




}
