using System;
using UnityEngine;
using System.Collections;
using PayBuild;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingPage : MonoBehaviour
{

	[SerializeField] private Slider _slider;
	public static LoadingPage Instance;

	//public ProgressBarNoMask ProgressBar;
	//public EasyFontTextMesh ProgressText;
	//public Transform roleSprite;

	public Text TipText;

	private float curProgress;
	private AsyncOperation Async;

	private void Awake() {
		if (Instance == null) {
			Instance = this;
		}
		else {
			Destroy(this);
		}
        
		DontDestroyOnLoad(this);
	}

	public void InitScene ()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
		PlayerData.Instance.SaveData();
		PublicSceneObject.Instance.Init ();
		//PublicSceneObject.Instance.IsReceiveAndroidBackKeyEvent = false;

		//CollectInfoEvent.SendEvent (CollectInfoEvent.EventType.Loading, "state", "start  level" + PlayerData.Instance.GetSelectedLevel()+" To:"+GlobalConst.SceneName);

		StartCoroutine ("IEInitScene");
	}

	private void SetProgress()
	{
		if (curProgress < 50)
			curProgress += 5;
		else
			++ curProgress;
		//ProgressText.text = curProgress + "%";
		float progressValue = curProgress / 100;
		_slider.value = progressValue;
	//	ProgressBar.SetProgress (progressValue);

		if (curProgress > 99) {
			//this.gameObject.SetActive(false);
			// var MainInterface=UIManager.Instance.UICanvas.GetChild(0).gameObject;
			// MainInterface.SetActive(true);
			// MainInterface.GetComponent<MainInterfaceControllor>().ClassicGameOnClick();
		}
		
		//roleSprite.localPosition = new Vector3 (-280 + progressValue * 560, -121);
	}

   
    IEnumerator IEInitScene()
	{
		TipText.text = TipsData.Instance.GetContent (1);

		AudioManger.Instance.StopAll();

		gameObject.SetActive (true);
		curProgress = 0;
		while(curProgress < 100)
		{
			SetProgress ();
			yield return new WaitForEndOfFrame ();
		}

		yield return new WaitForSeconds (0.2f);
		LoadSceneComplete ();
	}

	public void LoadScene()
	{
        //		if (PlayerData.Instance.GetNewPlayerState ())
        //			TipText.text = TipsData.Instance.GetContent (1);
        //		else
        //			TipText.text = TipsData.Instance.GetRandomContent ();

        //Clear the commission
        PlayerData.Instance.ClearPlayerDataChangeEvent ();
	//	PublicSceneObject.Instance.ClearAndroidBackKeyEvent ();
        //Save game data
        PlayerData.Instance.SaveData ();
		ExchangeActivityData.Instance.SaveData();

        //Game ready logo
        GlobalConst.IsReady = false;
		GlobalConst.IsUIReady = false;

		gameObject.SetActive (true);
		StartCoroutine ("IELoadScene");
	//	PublicSceneObject.Instance.IsReceiveAndroidBackKeyEvent = false;

	//	CollectInfoEvent.SendEvent (CollectInfoEvent.EventType.Loading, "state", "start level" + PlayerData.Instance.GetSelectedLevel ()+" To:"+GlobalConst.SceneName);
	}

	IEnumerator IELoadScene()
	{
		AudioManger.Instance.StopAll();
		
		curProgress = 0;
		Application.LoadLevelAsync (GlobalConst.SceneName.ToString ()).completed+=SceneLoadingComplete;
		while (curProgress < 100) {
			SetProgress ();
			yield return null;
		}
		
		
		
	}

	void SceneLoadingComplete(AsyncOperation operation) {
		if (operation.isDone) {
			switch (GlobalConst.SceneName) {
				case SceneType.UIScene:
					LoadUISceneComplete ();
					break;
				case SceneType.GameScene:
					LoadGameSceneComplete ();
					break;
			}
		}
	}
	
	
	/// <summary>
	/// 场景加载完成后调用此函数
	/// </summary>
	void LoadSceneComplete()
	{
		//CollectInfoEvent.SendEvent (CollectInfoEvent.EventType.Loading, "state", "end level" + PlayerData.Instance.GetSelectedLevel ()+" To:"+GlobalConst.SceneName);
	//	ExchangeActivityData.Instance.InitFromServer ();

		StopCoroutine ("IEWaiting");
		StartCoroutine ("IEWaiting");
	}

	/// <summary>
	/// 加载完成UI场景后调用此函数.
	/// </summary>
	void LoadUISceneComplete() {
		var MainScene = UiSceneLoader.instance.GetMainInterfaceController();
		UiSceneLoader.instance.SpawnPlayers();
		// var MainInterface=UIManager.Instance.UICanvas.GetChild(0).gameObject;
		// MainInterface.SetActive(true);
		//  MainInterface.GetComponent<MainInterfaceControllor>().ClassicGameOnClick();
		//
		// UIManager.Instance.ShowModule (UISceneModuleType.PropertyDisplay);
		// UIManager.Instance.ShowModule (UISceneModuleType.MainInterface);
		//
		// switch (GlobalConst.ShowModuleType) {
		// case UISceneModuleType.MainInterface:
		//
		// 	if (!GlobalConst.IsSignIn && PlayerData.Instance.GetCurrentChallengeLevel() > 1) {
		// 		UIManager.Instance.ShowModule (UISceneModuleType.SignIn);
		// 	}
		// 	break;
		// case UISceneModuleType.LockTip:
		// 	LockTipControllor.Instance.InitData(LockTipType.UnlockTip);
		// 	UIManager.Instance.ShowModule (UISceneModuleType.LockTip);
		// 	break;
		// case UISceneModuleType.LevelSelect:
		// 	LevelInfoControllor.Instance.SetModelIndex (IDTool.GetModelType (PlayerData.Instance.GetSelectedModel ()) - 1);
		// 	UIManager.Instance.ShowModule (UISceneModuleType.LevelSelect);
		// 	break;
		// case UISceneModuleType.LevelInfo:
		// 	LevelInfoControllor.Instance.SetModelIndex (IDTool.GetModelType (PlayerData.Instance.GetSelectedModel ()) - 1);
		// 	UIManager.Instance.ShowModule (UISceneModuleType.LevelSelect);
		// 	LevelInfoControllor.Instance.InitData (PlayerData.Instance.GetCurrentChallengeLevel());
		// 	UIManager.Instance.ShowModule (UISceneModuleType.LevelInfo);
		// 	break;
		// }
		AudioManger.Instance.PlayMusic(AudioManger.MusicName.UIBackground);
		Destroy(gameObject);
		//this.gameObject.SetActive(false);
	}

	IEnumerator IEWaiting()
	{
		// while (GlobalConst.IsReady == false || GlobalConst.IsUIReady == false) {
		// 	yield return new WaitForSeconds (0.2f);
		// }
		//PublicSceneObject.Instance.IsReceiveAndroidBackKeyEvent = true;
		gameObject.SetActive (false);

		switch (GlobalConst.SceneName) {
		case SceneType.UIScene:
			LoadUISceneComplete ();
			break;
		case SceneType.GameScene:
			LoadGameSceneComplete ();
			break;
		}

		yield return null;
	}
	
	/// <summary>
	/// 加载完成游戏场景后调用此函数.
	/// </summary>
	public void LoadGameSceneComplete() {
		
		//StartCoroutine(Delay());
		//Debug.Log("Game Scene Loaded");
		//GameUIManager.Instance.ShowModule (UISceneModuleType.GamePlayerUI);
		//SceneManager.LoadScene("GameScene");
		
	}

	IEnumerator Delay() {
		float currentTime = 95;
		
		while (curProgress < 100) {
			currentTime += Time.deltaTime;
			curProgress = currentTime;
			SetProgress();
			yield return null;
		}
		
		
	}

}