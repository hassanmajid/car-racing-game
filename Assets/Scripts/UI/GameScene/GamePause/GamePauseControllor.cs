using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GamePauseControllor : UIBoxBase {

	
	public override void Init ()
	{
		base.Init();
	}
	
	public override void Show ()
	{
		base.Show();
		transform.localPosition = GlobalConst.ShowPos;
		GameController.Instance.PauseGame();
	}
	
	public override void Hide ()
	{
		gameObject.SetActive (false);
		transform.localPosition = GlobalConst.LeftHidePos;
		GameController.Instance.ResumeGame();
		//GameUIManager.Instance.HideModule(UISceneModuleType.GamePause);

		GameUIManager.Instance.ShowModule(UISceneModuleType.GameResume);
	}
	
	public override void Back ()
	{
		AudioManger.Instance.PlaySound(AudioManger.SoundName.ButtonClick);
		Hide ();
	}
	
	public void ContinueBtnOnClick()
	{
		AudioManger.Instance.PlaySound(AudioManger.SoundName.ButtonClick);
		
		Hide();
	}

	public void RestartBtnOnClick()
	{
		AudioManger.Instance.PlaySound(AudioManger.SoundName.ButtonClick);
		GlobalConst.FirstIn = true;
		SceneManager.LoadScene("GameScene");
		// if(PlayerData.Instance.GetItemNum(PlayerData.ItemType.Strength) > 0)
		// {
		// 	PlayerData.Instance.ReduceItemNum(PlayerData.ItemType.Strength, 1);
		// 	GameController.Instance.RestartGame();
		// }
		// else
		// {
		// 	GiftPackageControllor.Instance.Show(PayType.PowerGift);
		// }

		// GameController.Instance.ResumeGame();
		// var currentScene = SceneManager.GetActiveScene();
		// SceneManager.UnloadSceneAsync(currentScene).completed+=onSceneUnloaded;
		
		
	}

	void onSceneUnloaded(AsyncOperation operation) {
		if (operation.isDone) {
			SceneManager.LoadScene("GameScene");
		}
	}
	
	
	public void BackMainUIBtnOnClick()
	{
		AudioManger.Instance.PlaySound(AudioManger.SoundName.ButtonClick);
		//自定义事件.
		//CollectInfoEvent.SendEvent(CollectInfoEvent.EventType.Level_Quit, "关卡模式", PlayerData.Instance.GetGameMode(), "选择关卡", "Level_" + PlayerData.Instance.GetSelectedLevel());

		//GlobalConst.ShowModuleType = UISceneModuleType.MainInterface;
		GlobalConst.SceneName = SceneType.UIScene;
		//LoadingPage.Instance.LoadScene ();
		int iCoinCount = Mathf.RoundToInt(GameData.Instance.curCoin * GameData.Instance.AddCoinPecent);
		PlayerData.Instance.AddItemNum(PlayerData.ItemType.Coin, iCoinCount);
		SceneManager.LoadScene("UIScene");
	}
	
}
