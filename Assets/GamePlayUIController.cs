using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GamePlayUIController : MonoBehaviour
{
   public static GamePlayUIController instance;
   public Action<AnimatorManager,bool> onResult;

   [SerializeField] private Camera mainCamera;
   [SerializeField] private Camera firstActorCamera;

   private bool isLeft = false;
   private bool isRight = false;
   
   [SerializeField] private GamePauseControllor gamePauseControllor;
   private GamePauseControllor gamePause;

   [SerializeField] private GameEndingScoreControllor _gameEndingScoreControllor;
   private GameEndingScoreControllor GameEndingScoreControllor;
   

   [SerializeField] private LoadingPage _loadingPage;
   private LoadingPage LoadingPage;
   
   [SerializeField] private GamePlayerUIControllor GamePLayUI;
   private GamePlayerUIControllor GamePlayerUIControllor;
   
   [SerializeField] private GameSkillControl GameSkillControl;
   private GameSkillControl _gameSkillControl;
   
   [SerializeField] private UIGuideControllor _guideControllor;
   private UIGuideControllor UIGuideControllor;
   
   
   [SerializeField] private Transform parent;
   [SerializeField] private Transform UIPanel;
   
   [SerializeField] private List<Sprite> propList;
   
   [SerializeField] private List<Sprite> actorList;

   [SerializeField] private List<Sprite> countDownList;
   
   [SerializeField] private List<Sprite> fruitsList;
   
   

   private Dictionary<string, Sprite> propSprite;
   private Dictionary<string, Sprite> actorSprite;
   private Dictionary<string, Sprite> countDownSprite;
   private Dictionary<string, Sprite> fruitsListSprite;


   private void Start() {
       onResult += ResultCameraChange;
   }


   private void OnDestroy() {
       onResult -= ResultCameraChange;
   }

   private AnimatorManager AnimatorManagerCar;
   void ResultCameraChange(AnimatorManager animatorManager,bool status) {
       GamePlayerUIControllor.gameObject.SetActive(false);
       UIPanel.gameObject.SetActive(false);
       mainCamera.cullingMask = 0;
       firstActorCamera.gameObject.SetActive(true);
       AnimatorManagerCar = animatorManager;
       if (status) {
           InvokeRepeating ("WinAnimation", 0, Random.Range(3.5f, 5.0f));
          
       }
       else {
           InvokeRepeating ("LoseAnimation", 0, Random.Range(3.5f, 5.0f));
       }
   }

   void WinAnimation() {
       AnimatorManagerCar.Win();
   }

   void LoseAnimation() {
       AnimatorManagerCar.Fail();
   }
   
   
   
   void SpawnGamePauseController() {
       gamePause = Instantiate(gamePauseControllor, parent);
   }
   void SpawnGameUI() {
       GamePlayerUIControllor=Instantiate(GamePLayUI, parent);
   }

   void SpawnGameEndingController() {
       GameEndingScoreControllor = Instantiate(_gameEndingScoreControllor, parent);
   }
   
   

   void SpawnLoading() {
       LoadingPage=Instantiate(_loadingPage, parent);
   }

    void SpawnGameSkillControl() {
       _gameSkillControl=Instantiate(GameSkillControl, parent);
   }

    public GameEndingScoreControllor GetGameEndingScoreController() {
        
        if(GameEndingScoreControllor==null)
            SpawnGameEndingController();

        return GameEndingScoreControllor;
    }
    
    
    
    public GamePauseControllor GetGamePauseControllerReference() {
       
        if(gamePause==null)
            SpawnGamePauseController();
       
        return gamePause;
    }

    
    public GamePlayerUIControllor GetGameUIControllerReference() {
       
        if(GamePlayerUIControllor==null)
            SpawnGameUI();
       
        return GamePlayerUIControllor;
    }
    
    

    public LoadingPage GetLoadingReference() {
       
        if(LoadingPage==null)
            SpawnLoading();
        GlobalConst.SceneName = SceneType.GameScene;
        
        return LoadingPage;
    }

    
    public GameSkillControl GetGameSkillControllerReference() {
       
        if(_gameSkillControl==null)
            SpawnGameSkillControl();
       
        return _gameSkillControl;
    }
   

   public UIGuideControllor GetGuideControllerReference() {
       
       if(UIGuideControllor==null)
           SpawnGuideController();
       
       return UIGuideControllor;
   }

    void SpawnGuideController() {
     UIGuideControllor = Instantiate(_guideControllor, parent);
       UIGuideControllor.gameObject.SetActive(false);
   }

    public void ShowGuideController() {
        UIGuideControllor.gameObject.SetActive(true);
    }
    
    public void HideGuideController() {
        UIGuideControllor.gameObject.SetActive(false);
    }
    
    
   void SetUpList() {
       propSprite = new Dictionary<string, Sprite>();
       
       foreach (var VARIABLE in propList) {
           propSprite.Add(VARIABLE.name,VARIABLE);
       }
       
       actorSprite = new Dictionary<string, Sprite>();
       
       foreach (var VARIABLE in actorList) {
           actorSprite.Add(VARIABLE.name,VARIABLE);
       }
       
       countDownSprite = new Dictionary<string, Sprite>();
       
       foreach (var VARIABLE in countDownList) {
           countDownSprite.Add(VARIABLE.name,VARIABLE);
       }
       
       
       fruitsListSprite = new Dictionary<string, Sprite>();
       
       foreach (var VARIABLE in fruitsList) {
           fruitsListSprite.Add(VARIABLE.name,VARIABLE);
       }
       
   }

   public Sprite GetPropSprite(string name) {
       return propSprite[name];
   }
   
   public Sprite GetFruitSprite(string name) {
       
       return fruitsListSprite[name];
   }
   
   
   

   public Sprite GetPlayerSprite(string name) {
       return actorSprite[name];
   }
   
   
   public Sprite GetCountDownSprite(string name) {
       return countDownSprite[name];
   }


   private void OnEnable() {
       SetUpList();
   }

   

   private void Awake() {
      if (instance == null) {
          instance = this;
      }
      else {
          Destroy(this);
      }
   }
   
   
   // public void LeftDown()
   // {
   //     isLeft = true;
   //     isRight = false;
   //     if (PlayerCarControl.Instance.carMove.xOffset >= PlayerCarControl.Instance.carMove.maxXOffset - 0.5f)
   //         return;
   //     PlayerCarControl.Instance.carMove.animManager.LeftMove();
   // }
   // public void LeftUp()
   // {
   //     isLeft = false;
   //     PlayerCarControl.Instance.carMove.animManager.LeftMoveBack();
   // }
   //
   // public void RightDown()
   // {
   //     isRight = true;
   //     isLeft = false;
   //     if (PlayerCarControl.Instance.carMove.xOffset <= PlayerCarControl.Instance.carMove.minXOffset + 0.5f)
   //         return;
   //     PlayerCarControl.Instance.carMove.animManager.RightMove();
   // }
   //
   // public void RightUp()
   // {
   //     isRight = false;
   //     PlayerCarControl.Instance.carMove.animManager.RightMoveBack();
   // }
   //
   //
   // void Update()
   // {
   //     if (isLeft)
   //     {
   //         PlayerCarControl.Instance.carMove.isTurnLeft = true;
   //         PlayerCarControl.Instance.carMove.isTurnRight = false;
   //     }
   //     else if (isRight)
   //     {
   //         PlayerCarControl.Instance.carMove.isTurnRight = true;
   //         PlayerCarControl.Instance.carMove.isTurnLeft = false;
   //     }
   //     else
   //     {
   //         PlayerCarControl.Instance.carMove.isTurnLeft = false;
   //         PlayerCarControl.Instance.carMove.isTurnRight = false;
   //     }
   //    
   // }

   
   
   
   
}
